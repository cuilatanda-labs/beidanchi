# Inmory - [词汇记忆体]

## Introduce

learn english vocabulary regularly with H.Ebbinghaus.

## Chinese Project Name

The following names can be use of this project.

* 英莫
* 英魔
* 一莫
* 易莫

## How to run?

* backend - go run main.go web --conf config.toml
* frontend(in www) - npm start

## Archetecture

* use [Gin](http://gin-gonic.com) as web framework.
* use [PostgreSQL]() as database.
* use [gorm]() as ORM framework.
* use [crypod]() as the Thirdty Queue Task Service.
* use [wire]() as IOC.



## Pg extension install

* create extension pg_trgm;
* create extension btree_gin;
* create index CONCURRENTLY idx_vocabulary_word on vocabularies USING GIN(word GIN_TRGM_OPS);
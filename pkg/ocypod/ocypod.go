package ocypod

import (
	"fmt"
	"time"

	"github.com/ddliu/go-httpclient"
	"github.com/laidingqing/beidanci/internal/config"
	"github.com/laidingqing/beidanci/pkg/logger"
)

type OcypodHelper struct {
	URL  string
	Name string
}

func NewOcypodHelper() *OcypodHelper {
	c := config.C.Ocypod

	return &OcypodHelper{
		URL:  c.Host,
		Name: c.Name,
	}
}

func (o *OcypodHelper) Run() {
	go func() {
		for {
			o.GetJob()
			logger.Infof("...")
			time.Sleep(time.Second)
		}
	}()
}

// 创建队列名称
func (o *OcypodHelper) CreateQueueName() {

}

// 获取队列ID
func (o *OcypodHelper) GetQueueID() {

}

// 获取任务
func (o *OcypodHelper) GetJob() {
	_, err := httpclient.Get(fmt.Sprintf("%s/%s/job", o.URL, o.Name), map[string]string{})
	if err != nil {
		logger.Infof("found ocypod error: %s", err.Error())
	}
}

// 获取任务
func (o *OcypodHelper) CreateJob() {
	_, err := httpclient.Post(fmt.Sprintf("%s/%s/job", o.URL, o.Name), map[string]string{})
	if err != nil {
		logger.Infof("found ocypod error: %s", err.Error())
	}
}

// 提交处理结果
func (o *OcypodHelper) SubmitJobResult() {

}

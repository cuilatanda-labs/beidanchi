import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { convertTags } from '../_helpers';
import { vocabActions } from '../_actions';
import { Layout } from '../_components/Layout';
import AppContext  from '../_components/Context';
import { VocabList } from '../_components/VocabList';

function NewPage() {
    const [inputs, setInputs] = useState({
        text: '',
    });
    const user = useSelector(state => state.authentication.user);

    const dispatch = useDispatch();
    const { text } = inputs;
    useEffect(() => {

    }, []);

    const newLink = {
        title: '创建',
        url: '/lists-new'
    }

    function handleChange(e) {
        const { name, value } = e.target;
        setInputs(inputs => ({ ...inputs, [name]: value }));
    }

    function handleSubmit(e){
        e.preventDefault();
        dispatch(vocabActions.grab(text));
    }

    return (
        <Layout>
            <div className='content-container page-panel'>
                <h1>添加记忆片段</h1>
                <div className='form-panel form-outline form-layout-flow form-size-wide'>
                    <form>
                        <p>计划。。。</p>
                        <h3>1. 指定一个标题，如:"古诗两首:静夜思,登鹳雀楼"，或"英语七年级上册第一单元"</h3>
                        <input type='text' placeholder='指定一个片段标题'/>
                        <h3>2. 给片段加入一段描述，便于管理内容</h3>
                        <textarea rows="2" style={{width: "100%"}} placeholder='指定一个描述'/>
                        <h3>3. 默认以词汇集为片段类型，背诵古诗等其它请选择文本类型</h3>
                        <input type="radio" name="catalog" value="1" checked="checked"/>词汇集
                        <input type="radio" name="catalog" value="0" style={{marginLeft: "10px"}}/>文本
                        <h3>4. 编辑片段内容</h3>
                        <div className='actions-panel'>
                            <VocabList />
                        </div>
                    </form>
                </div>
            </div>
        </Layout>
    );
}

export { NewPage };
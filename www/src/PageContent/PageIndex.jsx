import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Layout } from '../_components/Layout';
import { pageActions } from '../_actions';
import { Link } from 'react-router-dom';
import moment from 'moment';

function PageIndex() {
    const user = useSelector(state => state.authentication.user);
    const lists = useSelector(state => state.pages.lists) || [];
    // const pagination = useSelector(state => state.pages.lists.PageResult);
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(pageActions.list({}))
    }, []);

    return (
        <Layout>
            <div className='content-container'>
                <h1>记忆片段</h1>
                <div className='doc-text bb'>
                    将需要记忆的内容以小单元进行整理，以便能在今后的记忆曲线中反复回忆
                    <strong>怎么使用记忆片段!</strong>
                    <ul>
                        <li>将每日需要背诵的古诗、英语词汇集整理成为任务记忆片段。</li>
                        <li>记忆片段组成记忆曲线的内容，记忆曲线帮助您管理记忆任务。</li>
                    </ul>
                </div>
                <div className='actions-panel'>
                    <ul>
                        <li className='add'>
                            <Link to="/page-new">添加记忆片段</Link>
                        </li>
                    </ul>
                </div>
                <div className='page-list'>
                    { lists.map( item => <div key={item.id}>
                        <div className='page-item'>
                            <div className='title'>
                                <h2>{moment(item.created_at).format('YYYY MMMM Do')} {item.title}</h2>
                                <div className='text bb'>
                                    {item.desc}
                                </div>
                                <ul className="sections">
                                    <li className="category">记忆分类: { item.catalog === 0 && <span>文本</span>} { item.catalog === 1 && <span>词汇集</span>}</li>
                                    <li>创建于{moment(item.created_at).format("yyyy/MM/DD HH:mm")}</li>
                                    <li>最后更新于{moment(item.updated_at).format("yyyy/MM/DD HH:mm")}</li>
                                </ul>
                                <ul className='sections'>
                                    <li><a href="#">编辑</a></li>
                                    <li><a href="#">删除</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>)}

                </div>
            </div>
        </Layout>
    );
}

export { PageIndex };
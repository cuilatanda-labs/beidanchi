import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Layout } from '../_components/Layout';
import { userActions } from '../_actions';
import { useForm } from "react-hook-form";

function LoginPage() {
    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const loggingIn = useSelector(state => state.authentication.loggingIn);
    const dispatch = useDispatch();
    const location = useLocation();

    // reset login status
    useEffect(() => { 
        dispatch(userActions.logout()); 
    }, []);

    function onSubmit(data){
        if (data.email && data.password) {
            const { from } = location.state || { from: { pathname: "/" } };
            dispatch(userActions.login(data.email, data.password, from));
        }

        if( data.email && data.password == ''){
            console.log("发送登录链接。TODO")
        }

    }

    return (
        <Layout>
            <div className='centered-content limited-width login-content with-center-text'>
                <div className='center-form'>
                    <header>
                        <h2>Hello,</h2>
                        <h2>欢迎来到 Inmory.com</h2>
                    </header>
                    <form name="form" onSubmit={handleSubmit(onSubmit)} className="standard login">
                        <div className="field">
                            <input type="text" {...register("email", { required: true })} placeholder='输入邮箱作为登录账号' />
                        </div>
                        <div className="field">
                            <input type="password" {...register("password")} placeholder='密码(可选)' />
                            <p>如果您已遗失密码，或未设置密码，请将密码框留空，我们会通过电子邮件发送登录链接。</p>
                        </div>
                        <div className="actions">
                            <button className="green ss-plus" type="submit">
                                登 录
                            </button>
                        </div>
                        <div>还没有登录账号?<br/><a href="/register">注册一个免费账号，它能帮助你管理你的计划.</a></div>
                    </form>
                </div>
            </div>
        </Layout>
    );
}

export { LoginPage };
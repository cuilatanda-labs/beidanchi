import { vocabConstants } from '../_constants';

export function vocabularies(state = {}, action) {
    switch (action.type) {
        case vocabConstants.QUERY_REQUEST:
            return {
                loading: true
            };
        case vocabConstants.QUERY_SUCCESS:
            return {
                vocabs: action.data
            };
        case vocabConstants.QUERY_FAILURE:
            return {
                error: action.error
            };
        case vocabConstants.GRAB_WORD_REQUEST:
            return {
                loading: true
            };
        case vocabConstants.GRAB_WORD_SUCCESS:
            return {
                words: action.data
            };
        case vocabConstants.GRAB_WORD_FAILURE:
            return {
                error: action.error
            };            
        default:
            return state
    }
}
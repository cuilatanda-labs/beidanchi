import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { vocabularies } from './vocab.reducer';
import { common } from './common.reducer';
import { tasks } from './task.reducer';
import { pages } from './page.reducer';

const rootReducer = combineReducers({
    common,
    authentication,
    registration,
    users,
    vocabularies,
    pages,
    tasks,
    alert
});

export default rootReducer;
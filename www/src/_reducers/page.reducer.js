import { pageConstants } from '../_constants';

export function pages(state = {}, action) {
    switch (action.type) {
        case pageConstants.ALL_PAGES_REQUEST:
            return {
                loading: true
            };
        case pageConstants.ALL_PAGES_SUCCESS:
            return {
                lists: action.data.Data,
                pagination: action.data.PageResult
            };
        case pageConstants.ALL_PAGES_FAILURE:
            return {
                error: action.error
            };  
        case pageConstants.NEW_PAGES_REQUEST:
            return {
                loading: true
            };
        case pageConstants.NEW_PAGES_SUCCESS:
            return {
                id: action.data.id
            };
        case pageConstants.NEW_PAGES_FAILURE:
            return {
                error: action.error
            };  
        default:
            return state
    }
}
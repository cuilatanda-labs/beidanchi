import { commonConstants } from '../_constants';

export function common(state = {}, action) {
    switch (action.type) {
        case commonConstants.NAV_SELECTED:
            return { selected_key: action.key };
        default:
            return state
    }
}
import { taskConstants } from '../_constants';

export function tasks(state = {}, action) {
    switch (action.type) {
        case taskConstants.LIST_PLAN_REQUEST:
            return {
                loading: true
            };
        case taskConstants.LIST_PLAN_SUCCESS:
            return {
                list: action.data
            };
        case taskConstants.LIST_PLAN_FAILURE:
            return {
                error: action.error
            }; 
        default:
            return state
    }
}
import { commonConstants } from '../_constants';

export const userActions = {
    selected
};


function selected(key) {
    return { type: commonConstants.NAV_SELECTED, key };
}
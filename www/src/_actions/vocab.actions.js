import { vocabConstants } from '../_constants';
import { vocabService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const vocabActions = {
    search,
    grab
};

function search(vocab) {
    return dispatch => {
        dispatch(request({ vocab }));
        vocabService.searchVocab({ q: vocab })
            .then(
                data => { 
                    dispatch(success(data));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(data) { return { type: vocabConstants.QUERY_REQUEST, data } }
    function success(data) { return { type: vocabConstants.QUERY_SUCCESS, data } }
    function failure(error) { return { type: vocabConstants.QUERY_FAILURE, error } }
}

function grab(text) {
    return dispatch => {
        dispatch(request({ text }));
        vocabService.grabVocab(text)
            .then(
                data => { 
                    dispatch(success(data));
                    history.push("/add-vocab");
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(data) { return { type: vocabConstants.GRAB_WORD_REQUEST, data } }
    function success(data) { return { type: vocabConstants.GRAB_WORD_SUCCESS, data } }
    function failure(error) { return { type: vocabConstants.GRAB_WORD_FAILURE, error } }
}


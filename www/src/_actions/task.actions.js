import { taskConstants } from '../_constants';
import { taskService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const taskActions = {
    list
};

function list(params) {
    return dispatch => {
        dispatch(request(params));
        taskService.listToday(params)
            .then(
                data => { 
                    dispatch(success(data));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(data) { return { type: taskConstants.LIST_PLAN_REQUEST, data } }
    function success(data) { return { type: taskConstants.LIST_PLAN_SUCCESS, data } }
    function failure(error) { return { type: taskConstants.LIST_PLAN_FAILURE, error } }
}
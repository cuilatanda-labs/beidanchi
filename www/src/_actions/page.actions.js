import { pageConstants } from '../_constants';
import { pageService } from '../_services';
import { alertActions } from '.';
import { history } from '../_helpers';

export const pageActions = {
    list,
    create,
};

function list(date) {
    return dispatch => {
        dispatch(request(date));
        pageService.listPages({ date: date })
            .then(
                data => { 
                    dispatch(success(data));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(data) { return { type: pageConstants.ALL_PAGES_REQUEST, data } }
    function success(data) { return { type: pageConstants.ALL_PAGES_SUCCESS, data } }
    function failure(error) { return { type: pageConstants.ALL_PAGES_FAILURE, error } }
}


function create(data) {
    return dispatch => {
        dispatch(request(data));
        pageService.createPage(data)
            .then(
                data => { 
                    dispatch(success(data));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(data) { return { type: pageConstants.NEW_PAGES_REQUEST, data } }
    function success(data) { return { type: pageConstants.NEW_PAGES_SUCCESS, data } }
    function failure(error) { return { type: pageConstants.NEW_PAGES_FAILURE, error } }
}


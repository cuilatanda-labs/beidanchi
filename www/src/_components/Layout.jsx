import React from 'react';

import { Footer } from './Footer';
import { Header } from './Header';
import { Welcome } from './Welcome';

function Layout({children}) {
    return (
        <div>
            <Header />
            <Welcome />
            <div className='content-wrapper'>
                {children}
            </div>
            <Footer />
        </div>
    );
}

export { Layout };
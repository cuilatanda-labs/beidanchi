import React, { forwardRef, useEffect, useState } from 'react';
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { loginInfo } from '../_helpers/auth-header';
import { userActions } from '../_actions';

function VocabList() {
    const simple = {
        value: '',
        placeholder: "输入单词或短语，如: momery"
    }
    const [lists, setLists] = useState([simple, simple, simple, simple, simple, simple, simple, simple, simple, simple]);
    useEffect(() => {
        
    }, []);

    function addVocabItem(e) {
        lists.push(simple)
    }

    return (
        <ol className='vocab-list'>
            { lists.map( (item) => 
                <li className=''>
                    <input type="text" placeholder={item.placeholder}/>
                </li>
            )}
            
            <li className='add'>
                <a href="#" onClick={addVocabItem}>添加词汇</a>
            </li>
        </ol>
    );
}

export { VocabList };
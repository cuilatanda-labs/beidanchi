import React, { useEffect } from 'react';
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { loginInfo } from '../_helpers/auth-header';
import { userActions } from '../_actions';

const logo = require('../assets/logo-1wobq9i.png')

function Header() {
    const location = useLocation();
    const dispatch = useDispatch();
    const user = useSelector(state => state.authentication.user);

    useEffect(() => {
        
    }, []);

    function nav_selected(url) {
        if(location.pathname == url){
            return "selected"
        } 
    }

    function logout(){
        dispatch(userActions.logout())
    }

    return (
        <header className="page-header">
            <nav className="main">
                <ul>
                    <li title="首页" className="home">
                        <a href="/" className="logo"><span>Inmory</span></a>
                    </li>
                    <li title="整理记忆片段" className="page">
                        <a href="/pages" className={nav_selected('/pages')}><span>记忆片段</span></a>
                    </li>
                    <li title="记忆曲线" className="task">
                        <a href="/tasks" className={nav_selected('/tasks')}><span>记忆曲线</span></a>
                    </li>
                </ul>
            </nav>
        </header>
    );
}

export { Header };
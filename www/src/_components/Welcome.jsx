import React, { useEffect } from 'react';
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { loginInfo } from '../_helpers/auth-header';
import { userActions } from '../_actions';

function Welcome() {
    const location = useLocation();
    const dispatch = useDispatch();
    const user = useSelector(state => state.authentication.user);

    useEffect(() => {
        
    }, []);

    return (
        <div className="welcome-box">
            <ul>
                <li>Welcome!</li>
                <li><a href="/private/">我的账号</a></li>
            </ul>
        </div>
    );
}

export { Welcome };
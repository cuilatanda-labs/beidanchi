export * from './history';
export * from './store';
export * from './auth-header';


export function bindUrlParams(url, params){
    if(params){
        let paramsArray = [];
        Object.keys(params).forEach(key => {
            paramsArray.push(key+'='+params[key])
        })
        if(url.search(/\?/) === -1){
            url += "?"+paramsArray.join('&')
        }else{
            url += "&"+paramsArray.join('&')
        }
    }
    return url
}

export function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}

export function convertTags(str){
    // return 
    const strArr = str.split(" ")
    let tags = []
    strArr.map( (s) => {
        let name = ""
        if( s === 'zk') {
            name = "中考"
        }

        if( s === 'gk') {
            name = "高考"
        }

        if( s === 'cet4') {
            name = "四级"
        }

        if( s === 'cet6') {
            name = "六级"
        }

        if( s === 'ky') {
            name = "考研"
        }

        if( s === 'toefl') {
            name = "托福"
        }

        if( s === 'ielts') {
            name = "雅思"
        }

        if( s === 'gre') {
            name = "GRE"
        }

        tags.push(name)
    })
    return tags
}

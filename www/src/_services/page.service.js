import config from 'config';
import { authHeader, bindUrlParams, handleResponse } from '../_helpers';

export const pageService = {
    listPages,
    createPage,
};

function listPages(params) {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }
    };
    let url = bindUrlParams(`${config.apiUrl}/api/pages`, params)
    return fetch(url, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}

function createPage(data) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' },
        body: JSON.stringify(data)
    };
    let url = bindUrlParams(`${config.apiUrl}/api/pages`, undefined)
    return fetch(url, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
import config from 'config';
import { authHeader, bindUrlParams, handleResponse } from '../_helpers';

export const taskService = {
    listToday
};

function listToday(params) {
    console.log(params)
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }
    };
    let url = bindUrlParams(`${config.apiUrl}/api/tasks`, params)
    return fetch(url, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
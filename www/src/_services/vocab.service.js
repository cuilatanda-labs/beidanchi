import config from 'config';
import { authHeader, bindUrlParams, handleResponse } from '../_helpers';

export const vocabService = {
    searchVocab,
    grabVocab
};

function searchVocab(params) {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }
    };
    let url = bindUrlParams(`${config.apiUrl}/api/vocabularies`, params)
    return fetch(url, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}

function grabVocab(text) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' },
        body: JSON.stringify({ text })
    };
    let url = bindUrlParams(`${config.apiUrl}/api/vocabularies/grab`, undefined)
    return fetch(url, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}

import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Layout } from '../_components/Layout';
import { userActions } from '../_actions';
import { useForm } from "react-hook-form";

function RegisterPage() {
    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const [user, setUser] = useState({
        firstName: '',
        lastName: '',
        username: '',
        password: ''
    });
    const [submitted, setSubmitted] = useState(false);
    const registering = useSelector(state => state.registration.registering);
    const dispatch = useDispatch();

    // reset login status
    useEffect(() => {
        dispatch(userActions.logout());
    }, []);

    function handleChange(e) {
        const { name, value } = e.target;
        setUser(user => ({ ...user, [name]: value }));
    }

    function onSubmit(user){
        setSubmitted(true);
        if (user.email && user.password) {
            dispatch(userActions.register(user));
        }
    }

    return (
        <Layout>
            <div className='centered-content limited-width login-content with-center-text'>
                <div className='center-form'>
                    <header>
                        <h2>Hello,</h2>
                        <h2>欢迎注册 Inmory.com</h2>
                    </header>
                    
                    <form name="form" onSubmit={handleSubmit(onSubmit)} className="standard login">
                        <div className="field">
                            <input type="text" {...register("email", { required: true })} placeholder='输入邮箱作为登录账号' />
                        </div>
                        <div className="field">
                            <input type="password" {...register("password", { required: true })} placeholder='密码' />
                        </div>
                        <div className="actions">
                            <button className="green ss-plus" type="submit">
                                登 录
                            </button>
                        </div>
                        <div>
                            创建 Inmory.com 账户即表示您同意我们的服务条款。
                        </div>
                        <div>已有登录账号?<br/><a href="/login">点这里登录 Inmory.com.</a></div>
                    </form>
                </div>
            </div>
        </Layout>
    );
}

export { RegisterPage };
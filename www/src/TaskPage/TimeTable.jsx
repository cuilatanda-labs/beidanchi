import React, { useEffect, createContext } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { convertTags } from '../_helpers';
import { taskActions } from '../_actions';
import { Layout } from '../_components/Layout'
import AppContext from '../_components/Context';
import { mapKeys } from 'lodash'
import moment from 'moment';
import classNames from 'classnames';

moment.locale('zh-cn')

function TimeTable() {
    const user = useSelector(state => state.authentication.user);
    const tasks = useSelector(state => state.tasks.list) || {};
    const today = moment().format('YYYYMMDD');
    const before = moment().add(-7, 'days').format('YYYYMMDD')
    // console.log(moment().add(-7, 'days'), before)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(taskActions.list({s: before, e: today}))
    }, []);

    return (
        <div className='cards'>
            <table cellPadding='0' cellSpacing="0" className='timetable'>
                <thead>
                    <tr>
                        <th rowSpan={2} width="5%">序号</th>
                        <th rowSpan={2} width="5%">日期</th>
                        <th rowSpan={2} width="30%">词汇集</th>
                        <th rowSpan={2} width="5%">当天</th>
                        <th colSpan={8} width="50%">长期复习周期</th>
                    </tr>
                    <tr>
                        <th>1天</th>
                        <th>2天</th>
                        <th>4天</th>
                        <th>7天</th>
                        <th>15天</th>
                        <th>30天</th>
                        <th>3个月</th>
                        <th>6个月</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        Object.keys(tasks).map(t => {
                            const value = tasks[t]
                            return <tr key={t}>
                                <td>{t}</td>
                                <td>{moment(t, 'YYYYMMDD').format("YYYY-MM-DD")}</td>
                                <td>
                                    {value.pages.map( (p) => {
                                        return <span>{p.title}</span>
                                    })}
                                </td>
                                <td>7天</td>
                                <td>15天</td>
                                <td>30天</td>
                                <td>3个月</td>
                                <td>6个月</td>
                                <td>6个月</td>
                                <td>6个月</td>
                                <td>6个月</td>
                                <td>6个月</td>
                            </tr>
                        })
                    }
                </tbody>
            </table>
            <button type="button" className="button green ss-plus" key={'prev'}><span>上一周</span></button>
            <button type="button" className="button grey ss-plus" key={'next'}><span>下一周</span></button>
        </div>
    )
}

export { TimeTable };    
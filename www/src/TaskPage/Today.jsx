import React, { useEffect, createContext } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { convertTags } from '../_helpers';
import { vocabActions } from '../_actions';
import { Layout } from '../_components/Layout'
import AppContext from '../_components/Context';
import { truncate } from 'lodash'
import { taskActions } from '../_actions';
import moment from 'moment';

function TodayTask() {
    const user = useSelector(state => state.authentication.user);
    const today = moment().format('YYYYMMDD');
    const tasks = useSelector(state => state.tasks.list) || {};
    
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(taskActions.list({s: today, e: today}))
    }, []);

    return (
        <div className='cards'>
            { tasks[today] && tasks[today].map( (item) => 
                <section className='card' key={item.id}>
                    <div className='text bb time'>
                        <span>{moment(item.doing_at, 'YYYYMMDD').format("YYYY-MM-DD")}</span>
                    </div>
                    <h2>{item.title}</h2>
                    <ul>
                    {item.pages.map( (page) => 
                        <li className='content page-item' key={page.id}>
                            <h2>{page.title}</h2>
                            <p>{page.desc}</p>
                            <ul className="sections">
                                <li>创建于{moment(page.created_at).format("yyyy/MM/DD HH:mm")}</li>
                                <li>最后更新于{moment(page.updated_at).format("yyyy/MM/DD HH:mm")}</li>
                            </ul>
                        </li>
                    )}
                    </ul>
                    <div className='actions'>
                        <span className='plantime'></span>
                    </div>
                </section>
            )}
        </div>
    )
}

export { TodayTask };    
import React, { useEffect, createContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { convertTags } from '../_helpers';
import { vocabActions } from '../_actions';
import { Layout } from '../_components/Layout'
import AppContext  from '../_components/Context';
import { truncate } from 'lodash'
import { TodayTask } from './Today';
import { TimeTable } from './TimeTable';

function TaskIndex() {
    const user = useSelector(state => state.authentication.user);
    const [tabIndex, setTabIndex] = useState(0);

    const dispatch = useDispatch();

    useEffect(() => {

    }, []);

    function changeTabIndex(index){
        setTabIndex(index)
    }

    function selectedClass(index){
        if( index === tabIndex) return "selected";
        return ''
    }

    return (
        <Layout>
            <div className='content-container'>
                <h1>记忆曲线计划</h1>
                <div className='doc-text bb'>
                    通过制定记忆曲线内容，持续完成复习生词计划
                    <strong>怎么使用记忆片段!</strong>
                    <ul>
                        <li><strong>记忆曲线方法:</strong> 按当日、1天、2天、4天、7天、15天、1月、3月、6月周期复习</li>
                        <li><strong>规律复习计划:</strong> 提前为每日任务计划好适当、适量的词汇集，一天一个词汇集</li>
                        <li><strong>完备的时间表:</strong> 记忆曲线时间表计划提供在线复习及打印PDF复习</li>
                    </ul>
                </div>
                <div className='actions-panel'>
                    <ul>
                        <li className={selectedClass(0)}><a href="#" role={'button'} onClick={e => changeTabIndex(0)} key='0'>[今日复习]</a></li>
                        <li className={selectedClass(1)}><a href="#" role={'button'} onClick={e => changeTabIndex(1)} key='1'>[时间表]</a></li>
                    </ul>
                </div>

                <div className='reviews-content limited-width'>
                    { tabIndex == 0 && <TodayTask />}
                    { tabIndex == 1 && <TimeTable />}
                </div>

            </div>
        </Layout>
    );
}

export { TaskIndex };
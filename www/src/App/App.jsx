import React, { useEffect } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { HomePage } from '../HomePage';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';
import { PageIndex, NewPage } from '../PageContent'
import { TaskIndex } from '../TaskPage';

import '../_theme/style.less'

function App() {
    const alert = useSelector(state => state.alert);
    const dispatch = useDispatch();

    useEffect(() => {
        history.listen((location, action) => {
            dispatch(alertActions.clear());
        });
    }, []);

    return (
        <Router history={history}>
            <Switch>
                <Route exact key="home" path="/" component={HomePage} />
                <Route key="login" path="/login" component={LoginPage} />
                <Route key="register" path="/register" component={RegisterPage} />
                <Route exact key="pages" path="/pages" component={PageIndex} />
                <Route exact key="new-page" path="/page-new" component={NewPage} />
                <Route exact key="tasks" path="/tasks" component={TaskIndex}/>
            </Switch>
        </Router>
    );
}

export { App };
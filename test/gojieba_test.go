package test

import (
	"fmt"
	"testing"

	"github.com/yanyiwu/gojieba"
)

func TestJieba(t *testing.T) {
	x := gojieba.NewJieba()
	defer x.Free()
	// use_hmm := true
	// s := "Congress shall make no law respecting an establishment of religion, or prohibiting the free exercise thereof; or abridging the freedom of speech, or of the press, or the right of the people peaceably to assemble, and to petition the Government for a redress of grievances"
	s := "peaceably,right,exercise,petition,religion,law,make"
	wordinfos := x.Extract(s, 20)
	fmt.Println(s)
	fmt.Println("Tokenize:(搜索引擎模式)", wordinfos)
}

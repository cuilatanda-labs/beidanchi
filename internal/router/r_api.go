package router

import (
	"github.com/gin-gonic/gin"
	"github.com/laidingqing/beidanci/internal/router/middleware"
)

// RegisterAPI register api group router
func (a *Router) RegisterAPI(app *gin.Engine) {

	g := app.Group("/api")
	g.Use(middleware.RateLimiterMiddleware())

	g.GET("healthy", a.HealthAPI.Healthy)

	users := g.Group("users")
	{
		users.POST("", a.UserAPI.Registion)
		users.POST("login", a.LoginAPI.Login)
	}

	tasks := g.Group("tasks")
	{
		tasks.POST("", a.TaskAPI.Create)
		tasks.GET(":id", a.TaskAPI.Get)
		tasks.GET("", a.TaskAPI.Query)
	}

	pages := g.Group("pages")
	{
		pages.POST("", a.PageAPI.Create)
		pages.GET("", a.PageAPI.Query)
		pages.GET(":id", a.PageAPI.Get)
	}

}

package notify

type Sender interface {
	Send(to []string, title string, content string) error
}

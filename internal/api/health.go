package api

import (
	"github.com/gin-gonic/gin"
	"github.com/google/wire"
	"github.com/laidingqing/beidanci/internal/ginx"
)

// DemoSet 注入Demo
var HealthSet = wire.NewSet(wire.Struct(new(Health), "*"))

// Demo 示例程序
type Health struct {
}

// Query 查询数据
func (a *Health) Healthy(c *gin.Context) {
	ginx.ResSuccess(c, "Ok")
}

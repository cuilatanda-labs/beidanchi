package api

import (
	"github.com/gin-gonic/gin"
	"github.com/google/wire"
	"github.com/laidingqing/beidanci/internal/bll"
	"github.com/laidingqing/beidanci/internal/ginx"
	"github.com/laidingqing/beidanci/internal/schema"
	"github.com/laidingqing/beidanci/pkg/errors"
	"github.com/laidingqing/beidanci/pkg/logger"
)

// UserSet 注入User
var UserSet = wire.NewSet(wire.Struct(new(User), "*"))

// User 用户管理
type User struct {
	UserBll *bll.User
}

// Registion 用户注册
func (a *User) Registion(c *gin.Context) {
	ctx := c.Request.Context()
	var item schema.ReqestRegistion
	if err := ginx.ParseJSON(c, &item); err != nil {
		ginx.ResError(c, err)
		return
	} else if item.Password == "" {
		ginx.ResError(c, errors.New400Response("密码不能为空"))
		return
	}
	logger.Infof("start create user.")
	var user schema.User
	user.Email = item.Email
	user.Password = item.Password

	result, err := a.UserBll.Create(ctx, user)
	if err != nil {
		logger.Errorf("%v", err)
		ginx.ResError(c, err)
		return
	}
	ginx.ResSuccess(c, result)
}

// Get 查询指定数据
func (a *User) Get(c *gin.Context) {
	ctx := c.Request.Context()
	item, err := a.UserBll.Get(ctx, c.Param("id"))
	if err != nil {
		ginx.ResError(c, err)
		return
	}
	ginx.ResSuccess(c, item)
}

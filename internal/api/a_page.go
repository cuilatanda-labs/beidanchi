package api

import (
	"github.com/gin-gonic/gin"
	"github.com/google/wire"
	"github.com/laidingqing/beidanci/internal/bll"
	"github.com/laidingqing/beidanci/internal/ginx"
	"github.com/laidingqing/beidanci/internal/schema"
)

var PageSet = wire.NewSet(wire.Struct(new(Page), "*"))

// Page
type Page struct {
	PageBll *bll.Page
}

// Create
// create a page's entity
func (a *Page) Create(c *gin.Context) {
	ctx := c.Request.Context()
	var item schema.Page
	if err := ginx.ParseJSON(c, &item); err != nil {
		ginx.ResError(c, err)
		return
	}
	result, err := a.PageBll.Create(ctx, item)
	if err != nil {
		ginx.ResError(c, err)
		return
	}
	ginx.ResSuccess(c, result)
}

// Get
// get page information with content and his plan.
func (a *Page) Get(c *gin.Context) {
	ctx := c.Request.Context()
	pageID := c.Param("id")

	result, err := a.PageBll.Get(ctx, pageID)
	if err != nil {
		ginx.ResError(c, err)
		return
	}
	ginx.ResSuccess(c, result)
}

func (a *Page) Query(c *gin.Context) {
	ctx := c.Request.Context()
	param := schema.QueryPageParam{}
	result, err := a.PageBll.Query(ctx, param)
	if err != nil {
		ginx.ResError(c, err)
		return
	}
	ginx.ResSuccess(c, result)
}

package api

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/wire"
	"github.com/laidingqing/beidanci/internal/bll"
	"github.com/laidingqing/beidanci/internal/ginx"
	"github.com/laidingqing/beidanci/internal/schema"
	"github.com/laidingqing/beidanci/pkg/errors"
)

var TaskSet = wire.NewSet(wire.Struct(new(Task), "*"))

// Task 词汇集
type Task struct {
	TasksBll *bll.Task
}

func (a *Task) Create(c *gin.Context) {
	ctx := c.Request.Context()
	var item schema.TaskRequest
	if err := ginx.ParseJSON(c, &item); err != nil {
		ginx.ResError(c, err)
		return
	}

	result, err := a.TasksBll.Create(ctx, item)
	if err != nil {
		ginx.ResError(c, err)
		return
	}
	ginx.ResSuccess(c, result)
}

func (a *Task) Get(c *gin.Context) {
	ctx := c.Request.Context()
	id := c.Param("id")

	result, err := a.TasksBll.Get(ctx, id)
	if err != nil {
		ginx.ResError(c, err)
		return
	}
	ginx.ResSuccess(c, result)

}

func (a *Task) Query(c *gin.Context) {
	ctx := c.Request.Context()
	start := c.Query("s")
	end := c.Query("e")

	if start == "" {
		ginx.ResError(c, errors.ErrInvalidParamter)
		return
	}

	if end == "" {
		end = start
	}

	sDate, err := strconv.Atoi(start)
	if err != nil {
		ginx.ResError(c, err)
		return
	}

	eDate, err := strconv.Atoi(end)
	if err != nil {
		ginx.ResError(c, err)
		return
	}
	result, err := a.TasksBll.QueryTasks(ctx, schema.TaskQueryParam{
		Start: int64(sDate),
		End:   int64(eDate),
	})
	if err != nil {
		ginx.ResError(c, err)
		return
	}
	ginx.ResSuccess(c, result)
}

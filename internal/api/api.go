package api

import "github.com/google/wire"

// APISet 注入api
var APISet = wire.NewSet(
	HealthSet,
	LoginSet,
	UserSet,
	TaskSet,
	PageSet,
)

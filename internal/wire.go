//go:build wireinject
// +build wireinject

// The build tag makes sure the stub is not built in the final build.

package app

import (
	"github.com/google/wire"
	"github.com/laidingqing/beidanci/internal/api"
	"github.com/laidingqing/beidanci/internal/bll"
	"github.com/laidingqing/beidanci/internal/model/gormx/model"
	"github.com/laidingqing/beidanci/internal/router"
)

// BuildInjector 生成注入器
func BuildInjector() (*Injector, func(), error) {
	wire.Build(
		InitAuth,
		InitGormDB,
		InitGinEngine,
		model.ModelSet,
		bll.BllSet,
		api.APISet,
		router.RouterSet,
		InjectorSet,
	)
	return new(Injector), nil, nil
}

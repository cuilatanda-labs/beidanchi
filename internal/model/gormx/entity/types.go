package entity

type (
	PlanResult int
)

const (
	None PlanResult = iota + 1
	Reviewed
	Missed
)

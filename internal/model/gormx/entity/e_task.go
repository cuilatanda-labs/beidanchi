package entity

import (
	"context"
	"time"

	"github.com/laidingqing/beidanci/internal/schema"
	"github.com/laidingqing/beidanci/pkg/util/structure"
	"gorm.io/gorm"
)

func GetTaskDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return GetDBWithModel(ctx, defDB, new(Task))
}

// Task 艾宾浩斯遗忘曲线复习计划任务实体
// It include a schedule times and page's content.
type Task struct {
	ID        string    `gorm:"column:id;primary_key;size:36;"` // 内部编号
	Idx       int64     `gorm:"column:idx;"`                    // 用户内唯一，且增长
	Title     string    `gorm:"column:title;"`                  // 标题
	UserID    string    `gorm:"column:user_id;index"`           // 用户编号
	StartedAt time.Time `gorm:"column:started_at;index;"`       // 任务计划开始日期
	UpdatedAt time.Time `gorm:"column:updated_at;"`             // 更新日期
	DoingAt   int64     `gorm:"column:doing_at;<-:false"`       // 冗余字段
}

func (a Task) ToSchemaTask() *schema.Task {
	item := new(schema.Task)
	structure.Copy(a, item)
	return item
}

type SchemaTask schema.Task

func (a SchemaTask) ToTask() *Task {
	item := new(Task)
	structure.Copy(a, item)
	return item
}

type Tasks []*Task

func (a Tasks) ToSchemaTask() []*schema.Task {
	list := make([]*schema.Task, len(a))
	for i, item := range a {
		list[i] = item.ToSchemaTask()
	}
	return list
}

func GetTaskPeriodTimeDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return GetDBWithModel(ctx, defDB, new(TaskPeriodTime))
}

// TaskTime is a schedule that set task's priod review times.
type TaskPeriodTime struct {
	ID        string    `gorm:"column:id;primary_key;size:36;"` // 内部编号
	TaskID    string    `gorm:"column:task_id;"`                // task 编号
	DoingAt   int64     `gorm:"column:doing_at;index"`          // 任务回顾时间
	Days      int64     `gorm:"column:days;index"`          	// 所属回顾周期
	UpdatedAt time.Time `gorm:"column:updated_at;"`             // 更新日期
}

func (a TaskPeriodTime) ToSchemaTaskPeriodTime() *schema.TaskPeriodTime {
	item := new(schema.TaskPeriodTime)
	structure.Copy(a, item)
	return item
}

type SchemaTaskPeriodTime schema.TaskPeriodTime

func (a SchemaTaskPeriodTime) ToTaskPeriodTime() *TaskPeriodTime {
	item := new(TaskPeriodTime)
	structure.Copy(a, item)
	return item
}

type TaskPeriodTimes []*TaskPeriodTime

func (a TaskPeriodTimes) ToTaskPeriodTimes() []*schema.TaskPeriodTime {
	list := make([]*schema.TaskPeriodTime, len(a))
	for i, item := range a {
		list[i] = item.ToSchemaTaskPeriodTime()
	}
	return list
}

func GetTaskPageDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return GetDBWithModel(ctx, defDB, new(TaskPage))
}

// TaskPage is content list that include task's page .
type TaskPage struct {
	ID        string    `gorm:"column:id;primary_key;size:36;"` // 内部编号
	TaskID    string    `gorm:"column:task_id;"`                // task 编号
	PageID    string    `gorm:"column:page_id;index"`           // Page编号
	UpdatedAt time.Time `gorm:"column:updated_at;"`             // 更新日期
}

func (a TaskPage) ToSchemaTaskPage() *schema.TaskPage {
	item := new(schema.TaskPage)
	structure.Copy(a, item)
	return item
}

type SchemaTaskPage schema.TaskPage

func (a SchemaTaskPage) ToTaskPage() *TaskPage {
	item := new(TaskPage)
	structure.Copy(a, item)
	return item
}

type TaskPages []*TaskPage

func (a TaskPages) ToTaskPages() []*schema.TaskPage {
	list := make([]*schema.TaskPage, len(a))
	for i, item := range a {
		list[i] = item.ToSchemaTaskPage()
	}
	return list
}

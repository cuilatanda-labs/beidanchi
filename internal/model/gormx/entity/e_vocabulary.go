package entity

import (
	"context"

	"github.com/laidingqing/beidanci/internal/schema"
	"github.com/laidingqing/beidanci/pkg/util/structure"
	"gorm.io/gorm"
)

func GetVocabularyDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return GetDBWithModel(ctx, defDB, new(Vocabulary))
}

// Vocabulary 词汇实体
type Vocabulary struct {
	ID          string `gorm:"column:id;primary_key;size:36;"`
	Word        string `gorm:"column:word;index;"`             //词汇名，都以小写
	Phonetic    string `gorm:"column:phonetic;default:'';"`    //英音音标
	Definition  string `gorm:"column:definition;default:'';"`  //释义
	Translation string `gorm:"column:translation;default:'';"` //中文释义
	Pos         string `gorm:"column:pos;default:'';"`         //词频
	Collins     int64  `gorm:"column:collins;default:0;"`      //柯林斯星级
	Oxford      int64  `gorm:"column:oxford;default:0;"`       //是否是牛津三千核心词汇
	Tag         string `gorm:"column:tag;default:'';"`         //字符串标签
	Bnc         int64  `gorm:"column:bnc;default:0;index;"`    //英国国家语料库词频顺序
	Frq         int64  `gorm:"column:frq;default:0;"`          //当代语料库词频顺序
	Exchange    string `gorm:"column:exchange;default:'';"`    //时态复数等变换
	Detail      string `gorm:"column:detail;default:'';"`      //json 扩展信息 ,暂无
	Audio       string `gorm:"column:audio;default:'';"`       //读音音频，暂无
}

func (a Vocabulary) ToSchemaVocabulary() *schema.Vocabulary {
	item := new(schema.Vocabulary)
	structure.Copy(a, item)
	return item
}

// Vocabularies 实体列表
type Vocabularies []*Vocabulary

// ToSchemaUsers 转换为用户对象列表
func (a Vocabularies) ToSchemaVocabularies() []*schema.Vocabulary {
	list := make([]*schema.Vocabulary, len(a))
	for i, item := range a {
		list[i] = item.ToSchemaVocabulary()
	}
	return list
}

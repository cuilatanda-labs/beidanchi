package entity

import (
	"context"
	"time"

	"github.com/laidingqing/beidanci/internal/schema"
	"github.com/laidingqing/beidanci/pkg/util/structure"
	"gorm.io/gorm"
)

func GetPageDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return GetDBWithModel(ctx, defDB, new(Page))
}

// Page is content by memory.
type Page struct {
	ID        string             `gorm:"column:id;primary_key;size:36;"` // 内部编号
	UserID    string             `gorm:"column:user_id;index;"`          // 用户编号
	Title     string             `gorm:"column:title;"`                  // 标题
	Desc      string             `gorm:"column:desc;"`                   // 概要说明
	Catalog   schema.PageCatalog `gorm:"column:catalog;"`               // Page类型
	CreatedAt time.Time          `gorm:"column:created_at;"`             // 创建日期
	UpdatedAt time.Time          `gorm:"column:updated_at;"`             // 更新日期
}

func (a Page) ToSchemaPage() *schema.Page {
	item := new(schema.Page)
	structure.Copy(a, item)
	return item
}

type SchemaPage schema.Page

func (a SchemaPage) ToPage() *Page {
	item := new(Page)
	structure.Copy(a, item)
	return item
}

type Pages []*Page

func (a Pages) ToSchemaPage() []*schema.Page {
	list := make([]*schema.Page, len(a))
	for i, item := range a {
		list[i] = item.ToSchemaPage()
	}
	return list
}

func GetContentDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return GetDBWithModel(ctx, defDB, new(Content))
}

// Content
// It's a page's content that is rich text, have one record when catalog is text, have many records when catalog is list.
type Content struct {
	ID        string             `gorm:"column:id;primary_key;size:36;"` // 内部编号
	PageID    string             `gorm:"column:page_id;"`                // Page编号
	Catalog   schema.PageCatalog `gorm:"column:catalog;"`                // Page类型
	Content   string             `gorm:"column:content;"`                // 内容
	UpdatedAt time.Time          `gorm:"column:updated_at;"`             // 更新日期
	Ext01     string             `gorm:"column:ext_01;"`                 //扩展字段01
	Ext02     string             `gorm:"column:ext_02;"`                 //扩展字段02
	Ext03     string             `gorm:"column:ext_03;"`                 //扩展字段03
}

func (a Content) ToSchemaContent() *schema.Content {
	item := new(schema.Content)
	structure.Copy(a, item)
	return item
}

type SchemaContent schema.Content

func (a *SchemaContent) ToContent() *Content {
	item := new(Content)
	structure.Copy(a, item)
	return item
}

type Contents []*Content

func (a Contents) ToSchemaContent() []*schema.Content {
	list := make([]*schema.Content, len(a))
	for i, item := range a {
		list[i] = item.ToSchemaContent()
	}
	return list
}

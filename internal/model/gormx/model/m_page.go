package model

import (
	"context"

	"github.com/google/wire"
	"github.com/laidingqing/beidanci/internal/model/gormx/entity"
	"github.com/laidingqing/beidanci/internal/schema"
	"github.com/laidingqing/beidanci/pkg/errors"
	"github.com/laidingqing/beidanci/pkg/logger"
	"github.com/laidingqing/beidanci/pkg/util/uuid"
	"gorm.io/gorm"
)

var PageSet = wire.NewSet(wire.Struct(new(Page), "*"))

type Page struct {
	DB *gorm.DB
}

func (a *Page) CreatePage(ctx context.Context, item schema.Page) error {
	sitem := entity.SchemaPage(item)
	db := entity.GetPageDB(ctx, a.DB)
	result := db.Create(sitem.ToPage())
	return errors.WithStack(result.Error)
}

func (a *Page) GetPage(ctx context.Context, id string) (*schema.Page, error) {
	var item entity.Page
	db := entity.GetPageDB(ctx, a.DB)
	result := db.Where("id=?", id).First(&item)
	if result.Error != nil {
		return nil, errors.WithStack(result.Error)
	}

	page := item.ToSchemaPage()
	contents, err := a.GetPageContents(ctx, id)
	if err != nil {
		logger.Errorf("query page's content error: %v", err.Error())
	}
	page.Contents = contents

	return page, nil
}

func (a *Page) getQueryOption(opts ...schema.PageQueryOptions) schema.PageQueryOptions {
	var opt schema.PageQueryOptions
	if len(opts) > 0 {
		opt = opts[0]
	}
	return opt
}

func (a *Page) QueryPages(ctx context.Context, param schema.QueryPageParam, opts ...schema.PageQueryOptions) (*schema.PageQueryResult, error) {
	opt := a.getQueryOption(opts...)
	var items entity.Pages
	db := entity.GetPageDB(ctx, a.DB)
	if v := param.UserID; v != "" {
		db = db.Where("user_id=?", v)
	}
	opt.OrderFields = append(opt.OrderFields, schema.NewOrderField("created_at", schema.OrderByDESC))

	db = db.Order(ParseOrder(opt.OrderFields))
	pr, err := WrapPageQuery(ctx, db, param.PaginationParam, &items)

	if err != nil {
		return nil, errors.WithStack(err)
	}

	qr := &schema.PageQueryResult{
		PageResult: pr,
		Data:       items.ToSchemaPage(),
	}
	return qr, nil
}

func (a *Page) GetPageContents(ctx context.Context, id string) ([]*schema.Content, error) {

	var items entity.Contents
	db := entity.GetContentDB(ctx, a.DB)
	result := db.Where("page_id=?", id).Find(&items)
	if result.Error != nil {
		return nil, errors.WithStack(result.Error)
	}

	return items.ToSchemaContent(), nil
}

// CreateContents
// create content's record for page.
func (a *Page) CreateContents(ctx context.Context, items []*schema.Content, pageID string) error {
	list := make([]*entity.Content, len(items))
	for i, item := range items {
		sitem := entity.SchemaContent(*item)
		list[i] = sitem.ToContent()
		list[i].ID = uuid.NextID()
		list[i].PageID = pageID
	}
	db := entity.GetContentDB(ctx, a.DB)
	result := db.CreateInBatches(list, len(items))
	return errors.WithStack(result.Error)
}

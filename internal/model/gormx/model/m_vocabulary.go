package model

import (
	"context"

	"github.com/google/wire"
	"github.com/laidingqing/beidanci/internal/model/gormx/entity"
	"github.com/laidingqing/beidanci/internal/schema"
	"github.com/laidingqing/beidanci/pkg/errors"
	"gorm.io/gorm"
)

// VocabularySet 注入Vocabulary
var VocabularySet = wire.NewSet(wire.Struct(new(Vocabulary), "*"))

// Vocabulary 用户存储
type Vocabulary struct {
	DB *gorm.DB
}

func (a *Vocabulary) getQueryOption(opts ...schema.VocabularyQueryOptions) schema.VocabularyQueryOptions {
	var opt schema.VocabularyQueryOptions
	if len(opts) > 0 {
		opt = opts[0]
	}
	return opt
}

func (a *Vocabulary) QueryWord(ctx context.Context, word string) (*schema.Vocabulary, error) {
	var item entity.Vocabulary
	ok, err := FindOne(ctx, entity.GetVocabularyDB(ctx, a.DB).Where("word=?", word), &item)
	if err != nil {
		return nil, errors.WithStack(err)
	} else if !ok {
		return nil, nil
	}
	return item.ToSchemaVocabulary(), nil
}

// Query 查询数据
func (a *Vocabulary) Query(ctx context.Context, params schema.VocabularyQueryParam, opts ...schema.VocabularyQueryOptions) (*schema.VocabularyQueryResult, error) {
	opt := a.getQueryOption(opts...)

	db := entity.GetVocabularyDB(ctx, a.DB)
	if v := params.Term; v != "" {
		db = db.Where("word ~ ?", v)
	}
	db = db.Limit(20)
	opt.OrderFields = append(opt.OrderFields, schema.NewOrderField("id", schema.OrderByDESC))
	db = db.Order(ParseOrder(opt.OrderFields))

	var list entity.Vocabularies
	pr, err := WrapPageQuery(ctx, db, params.PaginationParam, &list)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	qr := &schema.VocabularyQueryResult{
		PageResult: pr,
		Data:       list.ToSchemaVocabularies(),
	}
	return qr, nil
}

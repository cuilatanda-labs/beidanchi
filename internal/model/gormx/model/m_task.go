package model

import (
	"context"

	"github.com/google/wire"
	"github.com/laidingqing/beidanci/internal/model/gormx/entity"
	"github.com/laidingqing/beidanci/internal/schema"
	"github.com/laidingqing/beidanci/pkg/errors"
	"gorm.io/gorm"
)

var TaskSet = wire.NewSet(wire.Struct(new(Task), "*"))

type Task struct {
	DB *gorm.DB
}

func (a *Task) CreateTask(ctx context.Context, item schema.Task) error {
	sitem := entity.SchemaTask(item)
	db := entity.GetTaskDB(ctx, a.DB)
	result := db.Create(sitem.ToTask())
	return errors.WithStack(result.Error)
}

func (a *Task) GetTask(ctx context.Context, id string) (*schema.Task, error) {
	var item entity.Task
	db := entity.GetTaskDB(ctx, a.DB)
	result := db.Where("id=?", id).First(&item)
	if result.Error != nil {
		return nil, errors.WithStack(result.Error)
	}
	return item.ToSchemaTask(), nil
}

func (a *Task) QueryTask(ctx context.Context, params schema.TaskQueryParam) ([]*schema.Task, error) {

	var items entity.Tasks
	db := entity.GetTaskPeriodTimeDB(ctx, a.DB)
	if params.Start > 20220000 && params.End > 20220000 {
		db = db.Where("task_period_times.doing_at >= ? and task_period_times.doing_at <= ?", params.Start, params.End)
	}

	result := db.Select("tasks.*, task_period_times.doing_at").Joins("left join tasks on tasks.id = task_period_times.task_id").Find(&items)
	if result.Error != nil {
		return nil, errors.WithStack(result.Error)
	}

	return items.ToSchemaTask(), nil
}

func (a *Task) CreateTaskPeriodTime(ctx context.Context, item schema.TaskPeriodTime) error {
	db := entity.GetTaskPeriodTimeDB(ctx, a.DB)
	sitem := entity.SchemaTaskPeriodTime(item)
	result := db.Create(sitem.ToTaskPeriodTime())
	return errors.WithStack(result.Error)
}

func (a *Task) CreateTaskPeriodTimes(ctx context.Context, items []schema.TaskPeriodTime) error {
	list := make([]*entity.TaskPeriodTime, len(items))
	for i, item := range items {
		sitem := entity.SchemaTaskPeriodTime(item)
		list[i] = sitem.ToTaskPeriodTime()
	}
	db := entity.GetTaskPeriodTimeDB(ctx, a.DB)
	result := db.CreateInBatches(list, len(items))
	return errors.WithStack(result.Error)
}

func (a *Task) GetTaskPeriodTimes(ctx context.Context, id string) ([]*schema.TaskPeriodTime, error) {
	var items entity.TaskPeriodTimes
	db := entity.GetTaskPeriodTimeDB(ctx, a.DB)
	result := db.Where("task_id=?", id).Find(&items)
	if result.Error != nil {
		return nil, errors.WithStack(result.Error)
	}

	return items.ToTaskPeriodTimes(), nil
}

func (a *Task) CreateTaskPages(ctx context.Context, items []schema.TaskPage) error {
	list := make([]*entity.TaskPage, len(items))
	for i, item := range items {
		sitem := entity.SchemaTaskPage(item)
		list[i] = sitem.ToTaskPage()
	}
	db := entity.GetTaskPageDB(ctx, a.DB)
	result := db.CreateInBatches(list, len(items))
	return errors.WithStack(result.Error)
}

func (a *Task) GetTaskPages(ctx context.Context, id string) ([]*schema.Page, error) {
	var items entity.Pages
	db := entity.GetTaskPageDB(ctx, a.DB)
	result := db.Where("task_id=?", id).Select("pages.*").Joins("left join pages on pages.id = task_pages.page_id").Find(&items)
	if result.Error != nil {
		return nil, errors.WithStack(result.Error)
	}

	return items.ToSchemaPage(), nil
}

func (a *Task) GetTaskByDay(ctx context.Context, date int64) ([]*schema.Task, error) {
	var items entity.Tasks
	db := entity.GetTaskPeriodTimeDB(ctx, a.DB)
	result := db.Where("task_period_times.doing_at=?", date).Select("tasks.*").Joins("left join tasks on tasks.id = task_period_times.task_id").Find(&items)
	if result.Error != nil {
		return nil, errors.WithStack(result.Error)
	}

	return items.ToSchemaTask(), nil
}

package model

import "github.com/google/wire"

// ModelSet model注入
var ModelSet = wire.NewSet(
	TransSet,
	UserSet,
	VocabularySet,
	TaskSet,
	PageSet,
)

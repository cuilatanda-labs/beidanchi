package gormx

import (
	"github.com/laidingqing/beidanci/internal/model/gormx/entity"
	"github.com/laidingqing/beidanci/pkg/logger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// Config 配置参数
type Config struct {
	Debug        bool
	DBType       string
	DSN          string
	MaxLifetime  int
	MaxOpenConns int
	MaxIdleConns int
	TablePrefix  string
}

// NewDB 创建DB实例
func NewDB(c *Config) (*gorm.DB, func(), error) {
	db, err := gorm.Open(postgres.Open(c.DSN), &gorm.Config{})
	if err != nil {
		return nil, nil, err
	}

	if c.Debug {
		db = db.Debug()
	}

	cleanFunc := func() {
		if err != nil {
			logger.Errorf("Gorm db close error: %s", err.Error())
		}
	}

	return db, cleanFunc, nil
}

// AutoMigrate 自动映射数据表
func AutoMigrate(db *gorm.DB) error {
	return db.AutoMigrate(
		new(entity.User),
		new(entity.Vocabulary),
		new(entity.Task),
		new(entity.TaskPage),
		new(entity.TaskPeriodTime),
		new(entity.Page),
		new(entity.Content),
	)
}

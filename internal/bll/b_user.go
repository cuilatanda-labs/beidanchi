package bll

import (
	"context"

	"github.com/google/wire"
	"github.com/laidingqing/beidanci/internal/model/gormx/model"
	"github.com/laidingqing/beidanci/internal/schema"
	"github.com/laidingqing/beidanci/pkg/errors"
	"github.com/laidingqing/beidanci/pkg/util/hash"
	"github.com/laidingqing/beidanci/pkg/util/uuid"
)

// UserSet 注入User
var UserSet = wire.NewSet(wire.Struct(new(User), "*"))

// User 用户管理
type User struct {
	// Enforcer      *casbin.SyncedEnforcer
	TransModel *model.Trans
	UserModel  *model.User
}

// Get 查询指定数据
func (a *User) Get(ctx context.Context, id string, opts ...schema.UserQueryOptions) (*schema.User, error) {
	item, err := a.UserModel.Get(ctx, id, opts...)
	if err != nil {
		return nil, err
	} else if item == nil {
		return nil, errors.ErrNotFound
	}
	return item, nil
}

// Create 创建数据
func (a *User) Create(ctx context.Context, item schema.User) (*schema.IDResult, error) {
	err := a.checkEmail(ctx, item)
	if err != nil {
		return nil, err
	}

	item.Password = hash.SHA1String(item.Password)
	item.ID = uuid.NextID()
	item.Status = 1
	err = a.TransModel.Exec(ctx, func(ctx context.Context) error {
		item.ID = uuid.NextID()
		return a.UserModel.Create(ctx, item)
	})
	if err != nil {
		return nil, err
	}

	return schema.NewIDResult(item.ID), nil
}

func (a *User) checkEmail(ctx context.Context, item schema.User) error {
	result, err := a.UserModel.Query(ctx, schema.UserQueryParam{
		PaginationParam: schema.PaginationParam{OnlyCount: true},
		Email:           item.Email,
	})
	if err != nil {
		return err
	} else if result.PageResult.Total > 0 {
		return errors.New400Response("用户名已经存在")
	}
	return nil
}

// Update 更新数据
func (a *User) Update(ctx context.Context, id string, item schema.User) error {
	oldItem, err := a.Get(ctx, id)
	if err != nil {
		return err
	} else if oldItem == nil {
		return errors.ErrNotFound
	}

	if item.Password != "" {
		item.Password = hash.SHA1String(item.Password)
	} else {
		item.Password = oldItem.Password
	}

	if item.RealName == "" {
		item.RealName = oldItem.RealName
	}

	if item.UserName == "" {
		item.UserName = oldItem.UserName
	}

	if item.Phone == "" {
		item.Phone = oldItem.Phone
	}

	item.ID = oldItem.ID
	item.Creator = oldItem.Creator
	item.CreatedAt = oldItem.CreatedAt
	item.Status = oldItem.Status
	item.Email = oldItem.Email

	err = a.TransModel.Exec(ctx, func(ctx context.Context) error {
		return a.UserModel.Update(ctx, id, item)
	})
	if err != nil {
		return err
	}

	return nil
}

package bll

import (
	"context"

	"github.com/google/wire"
	"github.com/laidingqing/beidanci/internal/model/gormx/model"
	"github.com/laidingqing/beidanci/internal/schema"
	"github.com/laidingqing/beidanci/pkg/auth"
	"github.com/laidingqing/beidanci/pkg/util/uuid"
)

var PageSet = wire.NewSet(wire.Struct(new(Page), "*"))

type Page struct {
	Auth       auth.Auther
	TransModel *model.Trans
	PageModel  *model.Page
}

// Create
func (a *Page) Create(ctx context.Context, item schema.Page) (*schema.IDResult, error) {
	item.ID = uuid.NextID()
	err := a.TransModel.Exec(ctx, func(ctx context.Context) error {
		err := a.PageModel.CreatePage(ctx, item)
		if err != nil {
			return err
		}
		contents := item.Contents
		return a.PageModel.CreateContents(ctx, contents, item.ID)
	})

	if err != nil {
		return nil, err
	}
	return schema.NewIDResult(item.ID), nil
}

// Get
func (a *Page) Get(ctx context.Context, id string) (*schema.Page, error) {
	result, err := a.PageModel.GetPage(ctx, id)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (a *Page) Query(ctx context.Context, param schema.QueryPageParam) (*schema.PageQueryResult, error) {

	result, err := a.PageModel.QueryPages(ctx, param)
	if err != nil {
		return nil, err
	}
	return result, nil
}

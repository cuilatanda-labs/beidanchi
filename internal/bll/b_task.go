package bll

import (
	"context"
	"strconv"
	"time"

	"github.com/google/wire"
	"github.com/laidingqing/beidanci/internal/model/gormx/model"
	"github.com/laidingqing/beidanci/internal/schema"
	"github.com/laidingqing/beidanci/pkg/auth"
	"github.com/laidingqing/beidanci/pkg/util/uuid"
)

var TaskSet = wire.NewSet(wire.Struct(new(Task), "*"))

type Task struct {
	Auth       auth.Auther
	TransModel *model.Trans
	TaskModel  *model.Task
}

// Create
// create a task for Ebbinghaus, when it's success it will genenate some data that including task's schedul time and content.
// It can't create a record of when the start date existed.
func (a *Task) Create(ctx context.Context, item schema.TaskRequest) (*schema.IDResult, error) {
	const TIME_LAYOUT = "20060102"
	day, _ := time.ParseDuration("24h")
	now := time.Now()
	startedAt, _ := time.Parse(TIME_LAYOUT, strconv.Itoa(item.Started))
	idx, _ := strconv.Atoi(startedAt.Format(TIME_LAYOUT))
	task := schema.Task{
		ID:        uuid.NextID(),
		Title:     item.Title,
		Idx:       int64(idx),
		StartedAt: startedAt,
		UpdatedAt: now,
	}
	days := []int{0, 1, 2, 4, 7, 15, 30, 90, 180}
	var list []schema.TaskPeriodTime
	for _, v := range days {
		// TODO mabey make error:  duplicate key
		id := uuid.NextID()
		t, _ := strconv.Atoi(startedAt.Add(day * time.Duration(v)).Format(TIME_LAYOUT))
		pt := schema.TaskPeriodTime{
			ID:        id,
			DoingAt:   int64(t),
			TaskID:    task.ID,
			Days: 	   int64(v),
			UpdatedAt: now,
		}
		list = append(list, pt)
	}

	var pages []schema.TaskPage
	for _, v := range item.Pages {
		pages = append(pages, schema.TaskPage{
			ID:        uuid.NextID(),
			TaskID:    task.ID,
			PageID:    v,
			UpdatedAt: time.Now(),
		})
	}

	err := a.TransModel.Exec(ctx, func(ctx context.Context) error {
		err := a.TaskModel.CreateTask(ctx, task)
		if err != nil {
			return err
		}

		err = a.TaskModel.CreateTaskPages(ctx, pages)
		if err != nil {
			return err
		}
		return a.TaskModel.CreateTaskPeriodTimes(ctx, list)
	})

	if err != nil {
		return nil, err
	}

	return schema.NewIDResult(task.ID), nil
}

func (a *Task) Get(ctx context.Context, id string) (*schema.Task, error) {
	task, err := a.TaskModel.GetTask(ctx, id)
	if err != nil {
		return nil, err
	}
	// wrap extend information that include period times and content.
	times, err := a.TaskModel.GetTaskPeriodTimes(ctx, id)
	if err != nil {
		return nil, err
	}
	task.PeriodTimes = times
	pages, err := a.TaskModel.GetTaskPages(ctx, id)
	if err != nil {
		return nil, err
	}
	task.Pages = pages
	// 这里要返回具体Page对象, TODO.

	return task, nil
}

func (a *Task) QueryTasks(ctx context.Context, query schema.TaskQueryParam) (map[int64][]*schema.Task, error) {

	result, err := a.TaskModel.QueryTask(ctx, query)
	if err != nil {
		return nil, err
	}

	for _, v := range result {
		pages, err := a.TaskModel.GetTaskPages(ctx, v.ID)
		if err == nil {
			v.Pages = pages
		}
	}

	return WrapDailyTasks(result, query), nil
}

func WrapDailyTasks(items []*schema.Task, query schema.TaskQueryParam) map[int64][]*schema.Task {
	result := make(map[int64][]*schema.Task)
	for i := query.Start; i <= query.End; i++ {
		result[i] = []*schema.Task{}
	}

	for _, v := range items {
		value, ok := result[v.DoingAt]
		if ok {
			value = append(value, v)
			result[v.DoingAt] = value
		}
	}

	return result
}

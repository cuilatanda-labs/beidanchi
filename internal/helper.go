package app

import (
	"github.com/laidingqing/beidanci/pkg/ocypod"
)

// InitQueueHelper 初始化Ocypod
func InitQueueHelper() (*ocypod.OcypodHelper, func(), error) {
	ocypod := ocypod.NewOcypodHelper()
	// Ocypod.Run()
	return ocypod, func() {

	}, nil
}

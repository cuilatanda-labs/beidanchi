package schema

import (
	"time"

	"github.com/laidingqing/beidanci/pkg/util/json"
	"github.com/laidingqing/beidanci/pkg/util/structure"
)

type ReqestRegistion struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type RequestLogin struct {
}

// User 用户对象
type User struct {
	ID        string    `json:"id"`         // 唯一标识
	UserName  string    `json:"user_name"`  // 用户名
	RealName  string    `json:"real_name"`  // 真实姓名
	Password  string    `json:"password"`   // 密码
	Phone     string    `json:"phone"`      // 手机号
	Email     string    `json:"email"`      // 邮箱
	Status    int       `json:"status"`     // 用户状态(1:启用 2:停用)
	Creator   string    `json:"creator"`    // 创建者
	CreatedAt time.Time `json:"created_at"` // 创建时间
}

type UserShow struct {
	ID        string    `json:"id"`
	UserName  string    `json:"user_name"`                             // 用户名
	RealName  string    `json:"real_name"`                             // 真实姓名
	Status    int       `json:"status" binding:"required,max=2,min=1"` // 用户状态(1:启用 2:停用)
	Creator   string    `json:"creator"`                               // 创建者
	CreatedAt time.Time `json:"created_at"`                            // 创建时间
}

func (a *User) String() string {
	return json.MarshalToString(a)
}

func (a *User) ToShow() UserShow {
	showItem := new(UserShow)
	structure.Copy(a, showItem)
	return *showItem
}

// Users 用户对象列表
type Users []*User

// ToIDs 转换为唯一标识列表
func (a Users) ToIDs() []string {
	idList := make([]string, len(a))
	for i, item := range a {
		idList[i] = item.ID
	}
	return idList
}

type UserShows []*UserShow

func (a Users) ToUserShow() UserShows {
	list := make(UserShows, len(a))
	for _, item := range a {
		showItem := new(UserShow)
		structure.Copy(item, showItem)
	}
	return list
}

// UserQueryOptions 查询可选参数项
type UserQueryOptions struct {
	OrderFields []*OrderField // 排序字段
}

// UserQueryParam 查询条件
type UserQueryParam struct {
	PaginationParam
	Email  string `form:"email"`  // 用户名
	Status int    `form:"status"` // 用户状态(1:启用 2:停用)
}

// UserQueryResult 查询结果
type UserQueryResult struct {
	Data       Users
	PageResult *PaginationResult
}

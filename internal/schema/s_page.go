package schema

import "time"

type PageCatalog int

const (
	PageText = iota
	PageList
)

type Page struct {
	ID        string      `json:"id"`                 // 内部编号
	UserID    string      `json:"user_id"`            // 用户编号
	Title     string      `json:"title"`              // 标题
	Desc      string      `json:"desc"`               //　概要说明
	Catalog   PageCatalog `json:"catalog"`            // Page类型
	CreatedAt time.Time   `json:"created_at"`         // 创建日期
	UpdatedAt time.Time   `json:"updated_at"`         // 更新日期
	Contents  []*Content  `json:"contents,omitempty"` // 内容
}

type Content struct {
	ID        string      `json:"id"`               // 内部编号
	PageID    string      `json:"page_id"`          // Page编号
	Catalog   PageCatalog `json:"catalog"`          // Page类型
	Content   string      `json:"content"`          // 内容
	UpdatedAt time.Time   `json:"updated_at"`       // 更新日期
	Ext01     string      `json:"ext_01,omitempty"` // 扩展字段01
	Ext02     string      `json:"ext_02,omitempty"` // 扩展字段02
	Ext03     string      `json:"ext_03,omitempty"` // 扩展字段03
}

type QueryPageParam struct {
	PaginationParam
	UserID string
}

type Pages []*Page

type PageQueryResult struct {
	Data       []*Page
	PageResult *PaginationResult
}

type PageQueryOptions struct {
	OrderFields []*OrderField // 排序字段
}

package schema

import "time"

type TaskRequest struct {
	Title   string   `json:"title"`
	Started int      `json:"started"`
	Pages   []string `json:"pages"`
}

type Task struct {
	ID          string            `json:"id"`
	Idx         int64             `json:"idx"`
	Title       string            `json:"title"`
	StartedAt   time.Time         `json:"started"`
	UpdatedAt   time.Time         `json:"updated"`
	UserID      string            `json:"user_id"`
	DoingAt     int64             `json:"doing_at"`
	PeriodTimes []*TaskPeriodTime `json:"period_times,omitempty"`
	Pages       []*Page           `json:"pages"`
}

type TaskQueryParam struct {
	PaginationParam
	Start int64 `form:"start"`
	End   int64 `form:"end"`
}

type TaskPeriodTime struct {
	ID        string    `json:"id"`
	TaskID    string    `json:"task_id"`
	DoingAt   int64     `json:"doing_at"`
	Days 	  int64 	`json:"days"`
	UpdatedAt time.Time `json:"updated_at"`
}

type TaskPage struct {
	ID        string    `json:"id"`         // 内部编号
	TaskID    string    `json:"task_id"`    // task 编号
	PageID    string    `json:"page_id"`    // Page编号
	UpdatedAt time.Time `json:"updated_at"` // 更新日期
	Title     string    `json:"title"`      //标题
}

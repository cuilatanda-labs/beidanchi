package schema

type Vocabulary struct {
	ID          string  `json:"id"`          //
	Word        string  `json:"word"`        //词汇名，都以小写
	Phonetic    string  `json:"phonetic"`    //英音音标
	Definition  string  `json:"definition"`  //释义
	Translation string  `json:"translation"` //中文释义
	Pos         string  `json:"pos"`         //词频
	Collins     int64   `json:"collins"`     //柯林斯星级
	Oxford      float64 `json:"oxford"`      //是否是牛津三千核心词汇
	Tag         string  `json:"tag"`         //字符串标签
	Bnc         int64   `json:"bnc"`         //英国国家语料库词频顺序
	Frq         int64   `json:"frq"`         //当代语料库词频顺序
	Exchange    string  `json:"exchange"`    //时态复数等变换
	Detail      string  `json:"detail"`      //json 扩展信息 ,暂无
	Audio       string  `json:"audio"`       //读音音频，暂无
}

type Vocabularies []*Vocabulary

// UserQueryOptions 查询可选参数项
type VocabularyQueryOptions struct {
	OrderFields []*OrderField // 排序字段
}

// UserQueryParam 查询条件
type VocabularyQueryParam struct {
	PaginationParam
	Term string `form:"term"` // 用户名
	Tag  string `form:"tag"`
}

// UserQueryResult 查询结果
type VocabularyQueryResult struct {
	Data       Vocabularies
	PageResult *PaginationResult
}

type GrabVocabRequest struct {
	Text string
}

# -*- coding: utf-8 -*-

import psycopg2
import pandas as pd
import shortuuid
from io import StringIO

def print_pandas_full(x):  
    pd.set_option('display.max_columns', None)
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows') 
    pd.reset_option('display.max_columns') 

conn = psycopg2.connect(database="goen", user="goen", password="goen", host="192.168.0.132", port="5432")
data = pd.read_csv('../data/advance.csv', encoding='utf-8', sep=",", low_memory=False)
data = data.fillna(value={'phonetic': '', 'definition':'', 'pos':'', 'tag':'', 'exchange':'', 'collins':0, 'oxford':0, 'bnc':0, 'frq':0, 'detail':'', 'audio':''})
data[['collins']] = data[['collins']].astype(int)
data[['oxford']] = data[['oxford']].astype(int)
data[['bnc']] = data[['bnc']].astype(int)
data[['frq']] = data[['frq']].astype(int)

print(len(data))
output = StringIO()
data.insert(0, "id", range(1, 1 + len(data)))
data.to_csv(output, sep='\t', index=False, header=False)
print(data)
cur = conn.cursor()
output1 = output.getvalue()
cur.copy_from(StringIO(output1), "vocabularies")
conn.commit()
cur.close()
conn.close()
# for index, row in data.iterrows():
#     cursor = conn.cursor()
#     id = shortuuid.uuid()
#     word = row['word'].lower()
#     phonetic = row['phonetic']
#     definition = row['definition']
#     translation = row['translation']
#     pos = row['pos']
#     collins = row['collins']
#     oxford = row['oxford']
#     tag = row['tag']
#     bnc = row['bnc']
#     frq = row['frq']
#     exchange = row['exchange']
#     sql = "insert into vocabularies(id, word, phonetic, definition, translation, pos, collins, oxford, tag, bnc, frq, exchange) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
#     cursor.execute(sql, (id, word, phonetic, definition, translation, pos, collins, oxford, tag, bnc, frq, exchange))
#     conn.commit()

# cursor.close()
# conn.close()